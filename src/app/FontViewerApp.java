package app;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class FontViewerApp {
    public static void main(String[] args) {

        MainFrame myFrame = new MainFrame();
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

class MainFrame extends JFrame {

    public MainFrame() {

        MainPanel myPanel = new MainPanel();
        add(myPanel);

        setBounds(590, 250, 800, 600);
        setLocationRelativeTo(null);
        setTitle("Font Viewer");
        setVisible(true);
        // pack();
    }
}

class MainPanel extends JPanel {
    public MainPanel() {
        setLayout(new BorderLayout());

        String[] fontList = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        myLabel = new JLabel(sampleText, JLabel.CENTER);
        fontSizeSlider = new JSlider(SwingConstants.HORIZONTAL, 5, 100, myTextFieldFont.getSize());
        fontFamilySpinner = new JSpinner(new SpinnerListModel(fontList) {
            public Object getNextValue() {
                return super.getPreviousValue();
            }

            public Object getPreviousValue() {
                return super.getNextValue();
            }
        });

        fontFamilySpinner.setFont(myTextFieldFont);
        fontFamilySpinner.setPreferredSize(new Dimension(400, 30));
        fontSizeSlider.setFont(myTextFieldFont);
        fontSizeSlider.setMajorTickSpacing(10);
        fontSizeSlider.setMinorTickSpacing(5);
        fontSizeSlider.setPaintTicks(true);
        fontSizeSlider.setPaintLabels(true);
        myLabel.setFont(new Font((String) fontFamilySpinner.getValue(), myTextFieldFont.getStyle(), myTextFieldFont.getSize()));

        add(fontFamilySpinner, BorderLayout.NORTH);
        add(fontSizeSlider, BorderLayout.SOUTH);
        add(myLabel, BorderLayout.CENTER);
        fontFamilySpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                myLabel.setFont(new Font((String) fontFamilySpinner.getValue(), myTextFieldFont.getStyle(), fontSizeSlider.getValue()));
            }
        });
        fontSizeSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                myLabel.setFont(new Font(myLabel.getFont().getName(), myLabel.getFont().getStyle(), fontSizeSlider.getValue()));
            }
        });
    }

    private final Font myTextFieldFont = new Font(this.getFont().getName(), this.getFont().getStyle(), 24);
    public static final String sampleText = "En un lugar de la mancha de cuyo nombre no quiero acordarme... ";
    private final JLabel myLabel;
    private final JSpinner fontFamilySpinner;
    private final JSlider fontSizeSlider;
}